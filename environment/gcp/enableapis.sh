#!/bin/bash

#bigquery-json.googleapis.com      BigQuery API
#cloudapis.googleapis.com          Google Cloud APIs
#clouddebugger.googleapis.com      Stackdriver Debugger API
#cloudtrace.googleapis.com         Stackdriver Trace API
#datastore.googleapis.com          Cloud Datastore API
#iam.googleapis.com                Identity and Access Management (IAM) API
#iamcredentials.googleapis.com     IAM Service Account Credentials API
#logging.googleapis.com            Stackdriver Logging API
#monitoring.googleapis.com         Stackdriver Monitoring API
#servicemanagement.googleapis.com  Service Management API
#serviceusage.googleapis.com       Service Usage API
#sql-component.googleapis.com      Cloud SQL
#storage-api.googleapis.com        Google Cloud Storage JSON API
#storage-component.googleapis.com  Cloud Storage

APIS="admin.googleapis.com cloudresourcemanager.googleapis.com compute.googleapis.com container.googleapis.com deploymentmanager.googleapis.com iam.googleapis.com"

gcloud services enable $APIS;
    
    
	
