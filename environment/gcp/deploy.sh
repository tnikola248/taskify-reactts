#!/bin/bash

set -e

PROJECT=$(gcloud config get-value project)

function apply_deployment () {
    if gcloud deployment-manager deployments describe $1; then
        gcloud deployment-manager deployments update $1 --delete-policy=ABANDON --config $2
     else
        gcloud deployment-manager deployments create $1 --config $2
    fi
}

set -e

apply_deployment kubernetes gce/${PROJECT}.yaml

