/// <reference types="cypress" />

describe("e2e test app", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000/");
  });
  it("should check common elements if exist in the DOM", () => {
    cy.get(".App").should("exist");
    cy.get(".heading").should("exist");
    cy.get(".input__box").should("exist");
    cy.get(".input_submit").should("exist");
    cy.get('[data-rbd-droppable-id="TodosList"]').should("exist");
    cy.get(".remove").should("exist");
  });

  it("should add ToDos", () => {
    // const arrayOfToDos = [
    //   "Wash the dishes",
    //   "Walk the dog",
    //   "Go grocery shopping",
    //   "Clean the room",
    //   "Make the bed",
    //   "Work on the project :)",
    //   "Go to the gym",
    //   "Prepare meals for the day",
    // ];
    cy.get(".input__box").type("Wash the dishes");
    cy.get(".input_submit").click();
    cy.wait(1000);

    cy.get(".input__box").type("Walk the dog");
    cy.get(".input_submit").click();
    cy.wait(1000);

    cy.get(".input__box").type("Clean the room");
    cy.get(".input_submit").click();
    cy.wait(1000);

    cy.get(".input__box").type("Work on the project :)");
    cy.get(".input_submit").click();
    cy.wait(1000);
    const dataTransfer = new DataTransfer();
    cy.get(".todos__single").eq(0).trigger("dragstart", { dataTransfer });

    cy.get(".remove").trigger("drop", { dataTransfer });
  });
});
